/*
File where we will configure the SNTP service to get the exact time.
*/

#include <WiFiUdp.h>


unsigned int localPort = udpPort;
char timeServer[] = "time.nist.gov";
byte packetBuffer[ NTP_PACKET_SIZE ];

WiFiUDP udp;


/* 
  Function to start SNTP.
  @return void
*/
void beginUdp(){;
    udp.begin(localPort);
    Serial.println("Started UDP");
}

/*
  Function to parse and calculate the time that we got from the ntp Server.
  @return unsigned long
*/

unsigned long getTimeNtp(){
    sendNTPpacket(timeServer);

    delay(1000);

    int cb = udp.parsePacket();
    if (!cb) {
        Serial.println("no packet yet");
    }
    else {
        udp.read(packetBuffer, NTP_PACKET_SIZE);
        // The timestamp starts at the byte 40 of the packet to the byte 43.
        unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
        unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
        unsigned long secsSince1900 = highWord << 16 | lowWord;
        Serial.print("EPOCH = ");
        // Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
        const unsigned long seventyYears = 2208988800UL;
        unsigned long epoch = (secsSince1900 - seventyYears) + daylightOffset_sec;
        Serial.println(epoch);

        // We print the human readable time just to Serial. 
        Serial.print("The UTC time is ");
        Serial.print((epoch  % 86400L) / 3600);
        Serial.print(':');
        if ( ((epoch % 3600) / 60) < 10 ) {
        Serial.print('0');
        }
        Serial.print((epoch  % 3600) / 60);
        Serial.print(':');
        if ( (epoch % 60) < 10 ) {
        Serial.print('0');
        }
        Serial.println(epoch % 60);
        return epoch;
    }
}

/* 
  Function to send an NTP request to the time server with the different parameters needed.
  @param address URL of the sntp server
  @return unsigned long
*/
unsigned long sendNTPpacket(char* address)
{
    // Parameters to send to the server.
    memset(packetBuffer, 0, NTP_PACKET_SIZE);
    packetBuffer[0] = 0b11100011;
    packetBuffer[1] = 0;
    packetBuffer[2] = 6;
    packetBuffer[3] = 0xEC;
    packetBuffer[12]  = 49;
    packetBuffer[13]  = 0x4E;
    packetBuffer[14]  = 49;
    packetBuffer[15]  = 52;

    udp.beginPacket(address, ntpPort);
    udp.write(packetBuffer, NTP_PACKET_SIZE);
    udp.endPacket();
}
