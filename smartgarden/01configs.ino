/*
File to define the configs used in system.
*/
 
// Global vars
#define MAX_PCT 100
#define MIN_PCT 0
#define MAX_VALUE 1023
#define MIN_VALUE 0

// Pins definition
#define DHTPIN 30
#define DHTTYPE DHT22
const int lightnessPin = 29;
const int soilHumidityPin = 28;
const int waterFlowPin = 12;
const int solenoidPin = 11;

// WiFI data
//char wifi_ssid[] = "TP-LINK_5C6224";
char wifi_ssid[] = "MIWIFI_2G_xut9";
//char wifi_password[] = "57477640";
char wifi_password[] = "d2ub2hkindvt";
const char test_server[] = "80.211.155.16";
const int test_connection_port = 22;

// MQTT config
const char mqtt_server[] = "80.211.155.16";
const uint16_t mqtt_port = 1883;
const char mqtt_id[] = "Garden1";
char humTopic[] = "/smartgarden/garden1/env_humidity";
char tempTopic[] = "/smartgarden/garden1/env_temperature";
char lightTopic[] = "/smartgarden/garden1/lightness";
char soilHumTopic[] = "/smartgarden/garden1/soil_humidity";
char waterFlowTopic[] = "/smartgarden/garden1/waterflow";
char remoteWaterActivationTopic[] = "/smartgarden/garden1/start_water";

// Waterflow sensor config
const int waterFlowFactor = 7.5;

// SNTP config
const int NTP_PACKET_SIZE = 48;
const int udpPort = 2390;
const int ntpPort = 123;
const int daylightOffset_sec = 3600;
