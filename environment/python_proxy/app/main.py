import config
import eventlet
import requests
import json
import paho.mqtt.client as mqttClient
from datetime import datetime
from flask import Flask, request
from flask_mqtt import Mqtt
from flask_socketio import SocketIO
from flask_bootstrap import Bootstrap
from influxdb import InfluxDBClient


eventlet.monkey_patch()

# Flask APP configuration
app = Flask(__name__)
app.config['MQTT_BROKER_URL'] = config.MQTT_SERVER
app.config['MQTT_BROKER_PORT'] = config.MQTT_PORT

mqtt = Mqtt(app)
socketio = SocketIO(app)
bootstrap = Bootstrap(app)

# Influx client configuration.
influxdb_client = InfluxDBClient(config.INFLUXDB_ADDRESS,
                                 config.INFLUXDB_PORT,
                                 config.INFLUXDB_USER,
                                 config.INFLUXDB_PASSWORD,
                                 None)


# Function that in the future will allow to call and endpoint and activate the water flow.
@app.route("/start_water")
def start_water():
    mqtt.publish(config.MQTT_REMOTE_WATER_TOPIC, config.START_WATER_MESSAGE)
    return "This should start the water flow."


# Function to connect to MQTT topics.
@mqtt.on_connect()
def handle_connect(client, userdata, flags, rc):
    print("Connecting to MQTT all topics")
    mqtt.subscribe(config.MQTT_TOPIC_PREFIX)
    print("Connected to all topics")


# Function that will be called on every message received.
@mqtt.on_message()
def handle_mqtt_message(client, userdata, message):
    print("Message received")
    message_data = message.payload.decode().split("|")
    message_time = datetime.fromtimestamp(int(message_data[0])).strftime('%Y-%m-%dT%H:%M:%SZ')
    message_value = message_data[1]
    topic_data = message.topic.split("/")
    metric = topic_data[3]
    metric_id = topic_data[2]

    # Json that we will send to the InfluxDB
    json_body = [
        {
            "measurement": metric,
            "tags": {
                "ID": metric_id
            },
            "time": message_time,
            "fields": {
                "value": message_value
            }
        }
    ]
    influxdb_client.write_points(json_body)
    print("Message wrote in InfluxDB")


# Function to connect to InfluxDB
def init_influxdb_database():
    print("Connecting to InfluxDB")
    databases = influxdb_client.get_list_database()
    if len(list(filter(lambda x: x['name'] == config.INFLUXDB_DATABASE, databases))) == 0:
        influxdb_client.create_database(config.INFLUXDB_DATABASE)
    influxdb_client.switch_database(config.INFLUXDB_DATABASE)
#    influxdb_client.create_retention_policy(config.INFLUXBD_RETENTION_POLICY_NAME, config.INFLUXBD_RETENTION_TIME, '1', default=True)
    print("Connected to InfluxDB")


# Main function
if __name__ == '__main__':
    init_influxdb_database()
    socketio.run(app, host='0.0.0.0', port=5000, use_reloader=True, debug=True)
